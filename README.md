# Compiling, bulding image, pushing to Gitlab Registry simple Java project(Hello World) 

## My gitlab-ci.yml file
```
# three stages of pipeline
stages:
  - package
  - build
  - test

# create variables 
variables:
  JENKINS_IMAGE: "$CI_REGISTRY_IMAGE:1.0.${CI_PIPELINE_ID}"
  LATEST_IMAGE: $CI_REGISTRY_IMAGE:latest
  INT: '8080'
  EXT: '8090'
  CONTAINER_NAME: 'jenkins_husak'

# First stage : compiling, testing, packaging war file and pass artifact to next job. Job runs in maven container on shared runner
mvn_package_job:
  stage: package
  when: manual
  image: maven:3.6-jdk-8
  script:
    - mvn clean
    - mvn compile
    - mvn test
    - mvn package
  artifacts:
    paths:
    - webapp/target/*.war

# Second stage: login to Gitlag Registry and building image with my project from local Dockerfile    
build_image_job:
  stage: build
  when: manual
  image: docker
  services:
    - docker:dind
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $JENKINS_IMAGE .
    - docker push $JENKINS_IMAGE

# Third stage:testing of running container with curl and then pushing one container with version tag and second with latest tag to Github Registry
test_and_push_job:
  stage: test
  when: manual
  image: docker
  services:
    - docker:dind
  script:
    - apk add --no-cache curl
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $JENKINS_IMAGE 
    - docker run -d --name $CONTAINER_NAME -p $EXT:$INT $JENKINS_IMAGE 
    - sleep 20
    - curl http://docker:8090/webapp/
    - docker stop $CONTAINER_NAME 
    - docker rm $CONTAINER_NAME 
    - docker push $JENKINS_IMAGE
    - docker tag $JENKINS_IMAGE $LATEST_IMAGE
    - docker push $LATEST_IMAGE



```

***

